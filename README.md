About
=====
- Sample Django project with the 'client' app in core
- Built with latest Django (v1.4)
- Layout and effect based on Twitter Bootstrap (v2.0.4)
- CRUD in admin page only
- Click 'Send mail' to send mail invidually or 
  'Send mails' to send mails in bulk. See email
  content in terminal.

Setup
=====
$ virtualenv env 
$ source env/bin/activate
$ cd project-source-code/
$ pip install -r requirements.txt
$ python manage.py syncdb --noinput  # fixtures loaded
$ python manage.py runserver         # superuser: admin/admin
