/*
 * Override Bootstrap .modal
 * Support ajax submit
 * Require jquery.form.js
 */
(function($) {
    var Modal = function(el, options) {
        this.el = el;
        this.options = $.extend($.fn.modal.defaults, options);
        return this;
    }

    Modal.prototype = {
        open: function () {
            var self = this;
            this.$modal = $("#modal-popup");
            this.populate(this.el.attr("href"));
            if (this.options.backdrop) {
                this.$backdrop = $('<div id="modal-backdrop" class="modal-backdrop" />');
                this.$backdrop.click(function () {
                    self.close();
                });
                this.$backdrop.appendTo("#modal");
            }
            this.$modal.delegate(".modal-close", "click", function (e) {
                e.preventDefault();
                self.close();
            });
            $("#modal").fadeIn(250);
        },

        close: function () {
            var self = this;
            if (this.close_redirect) {
                window.location.href = this.close_redirect;
            } else {
                $("#modal").fadeOut(250, function() {
                    $("#modal").remove();
                    self.$modal = null;
                    if (self.options.backdrop) {
                        self.$backdrop = null;
                    }
                });
            }
        },

        ajax: function (modal) {
            var self = this;
            modal.find("a.ajax").click(function(e) {
                e.preventDefault();
                self.populate($(this).attr("href"));
            }).removeClass("ajax");

            $("form.ajax", modal).ajaxForm({
                dataType: "json",
                success: function (data) {
                    if (data.html) {
                        if (data.location) {
                            self.close_redirect = data.location;
                        }
                        modal.html(data.html);
                        self.ajax(self.$modal);
                    } else {
                        if (data.location) {
                            window.location.href = data.location;
                        }
                    }
                }
            });
        },

        populate: function(url) {
            var self = this;
            this.$modal.load(url, function() {
                self.ajax(self.$modal);
                self.el.trigger('loaded');
            });
        },
    }

    $.fn.modal = function(options) {
        options = options || {};
        var modal = new Modal(this, options);
        $(this).click(function(e) {
            e.preventDefault();
            if ($(this).hasClass('disabled')) return false;
            $("body").append('<div id="modal" style="display: none;"></div>');
            $("#modal").append('<div id="modal-popup" class="modal"></div>');
            modal.open();
        });
        return modal;
    }

    $.fn.modal.defaults = {
        backdrop: true,
    }
})(jQuery)
