from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils import simplejson as json
from django.contrib import messages
from django.core.urlresolvers import reverse

from client.models import Client, Message
from client.forms import ClientForm, MessageForm

try:
    from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView, RedirectView
except ImportError:
    try:
        from cbv import ListView, DetailView, CreateView, UpdateView, DeleteView, RedirectView
    except ImportError:
        raise ImportError("Django==1.3+ or django-cbv==0.1.5 required.")


"""
Object views: Detail, Create, Update, Delete
List views: List
Read views: List, Detail
Write views: Create, Update, Delete
"""


class ClientViewMixin(object):
    """Common for all views."""

    model = Client
    form_class = ClientForm
    paginate_by = 10

    def get_template_names(self):
        if self.request.is_ajax():
            try:
                return [self.template_name_ajax]
            except AttributeError:
                return [self.template_name]
        else:
            return [self.template_name]

    def get_object(self, queryset=None):
        pk = self.kwargs.get("pk")
        slug = self.kwargs.get("slug")

        if slug is None:  # RedirectView reaches here
            return get_object_or_404(self.model, pk=pk)
        else:
            return get_object_or_404(self.model, pk=pk, slug=slug)

    def get_query_string(self):
        """Get query string to append to success_url."""
        try:
            return self.request.session['query_string']  # saved in list view
        except KeyError:
            return self.request.META['QUERY_STRING']

    def get_success_url(self):
        """Common for write views. No need for read views."""
        if self.success_url:
            return self.success_url
        return ('%s?%s' % (self.object.get_list_url(), self.get_query_string())).strip('?')


class ClientListView(ClientViewMixin, ListView):
    template_name = "client/list.html"

    def get(self, request, *args, **kwargs):
        request.session['query_string'] = self.request.META['QUERY_STRING']
        return super(ClientListView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        try:
            ctx = super(ClientListView, self).get_context_data(**kwargs)
        except Http404:
            # The self.paginate_queryset() will catch django.core.paginator.InvalidPage
            # and then will raise Http404. Use the last page to get better ux.
            self.kwargs['page'] = 'last'
            ctx = super(ClientListView, self).get_context_data(**kwargs)
        return ctx


class MessageViewMixin(ClientViewMixin):
    model = Message
    form_class = MessageForm


class MessageCreateView(MessageViewMixin, CreateView):
    template_name = "client/message_create.html"
    template_name_ajax = "client/message_create_ajax.html"
    template_name_message = "client/message_create_message.txt"

    def form_valid(self, form):
        recipients = Client.objects.filter(pk__in=form.cleaned_data['recipients_ids'].split(','))

        if recipients.count() == 0:
            # Shouldn't reach this
            return self.form_invalid(form)

        self.object = message = form.save()
        message.recipients = recipients

        # Send email
        from django.core.mail import send_mail
        from django.conf import settings
        emails = message.recipients_emails()
        if len(emails) > 0:
            send_mail(message.subject, message.body, settings.DEFAULT_FROM_EMAIL, emails)
            message.is_sent = True
            message.save()

        messages.success(self.request, render_to_string(self.template_name_message))

        if self.request.is_ajax():
            data = {
                "status": "success",
                "location": self.get_success_url(),
            }
            return HttpResponse(json.dumps(data), content_type="application/json")
        else:
            return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        if self.request.is_ajax():
            ctx = RequestContext(self.request, self.get_context_data(form=form))
            data = {
                "status": "failed",
                "html": render_to_string(self.get_template_names(), ctx),
            }
            return HttpResponse(json.dumps(data), content_type="application/json")
        else:
            return self.render_to_response(self.get_context_data(form=form))

    def get_success_url(self):
        return reverse("client_list")
