from django.conf.urls.defaults import *

from client.views import ClientListView, MessageCreateView


urlpatterns = patterns("",
    url(r"^$", ClientListView.as_view(), name="client_list"),
    url(r"^message/create/$", MessageCreateView.as_view(), name="message_create"),
)
