from django import forms

from client.models import Client, Message
from client.widgets import ReadOnlyWidget


class ClientForm(forms.ModelForm):
    """For create and update"""
    class Meta:
        model = Client
        exclude = ['user', 'slug']


class MessageForm(forms.ModelForm):
    recipients_names = forms.CharField(label='Recipients', widget=ReadOnlyWidget)
    recipients_ids = forms.CharField(widget=forms.HiddenInput)

    class Meta:
        model = Message
        exclude = ('recipients', 'is_sent')
        fields = ('recipients_names', 'recipients_ids', 'subject', 'body')

    def __init__(self, *args, **kwargs):
        self.recipients = kwargs.pop('recipients', None)
        super(MessageForm, self).__init__(*args, **kwargs)

        if self.recipients:
            names = [r.name for r in self.recipients]
            ids = [str(r.pk) for r in self.recipients]
            self.fields['recipients_names'].initial = ', '.join(names)
            self.fields['recipients_ids'].initial = ','.join(ids)
