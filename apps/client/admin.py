from django.contrib import admin

from client.models import Client, Message


class ClientAdmin(admin.ModelAdmin):
    pass


class MessageAdmin(admin.ModelAdmin):
    pass


admin.site.register(Client, ClientAdmin)
admin.site.register(Message, MessageAdmin)
