from django.db import models
from django.contrib.auth.models import User


class Client(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField(max_length=255)
    phone = models.CharField(max_length=255, blank=True)

    def __unicode__(self):
        return self.name


class Message(models.Model):
    recipients = models.ManyToManyField(Client)
    subject = models.CharField(max_length=255)
    body = models.TextField(blank=True)
    is_sent = models.BooleanField(default=False)
    # TODO: created_date, sent_date

    def __unicode__(self):
        return '%s: %s' % (', '.join(self.recipients_names()), self.subject)

    def recipients_names(self):
        return self.recipients.values_list('name', flat=True)

    def recipients_emails(self):
        return self.recipients.values_list('email', flat=True)
