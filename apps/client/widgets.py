from django import forms


class ReadOnlyWidget(forms.TextInput):
    def __init__(self, *args, **kwargs):
        kwargs["attrs"] = {
            "readonly": "readonly",
            "class": "disabled"
        }
        super(ReadOnlyWidget, self).__init__(*args, **kwargs)
